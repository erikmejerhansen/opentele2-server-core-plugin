databaseChangeLog = {
    changeSet(author: "RA (kih-1842)", id: "kih-1842-1") {
        preConditions(onFail: 'MARK_RAN') {
            not {
                tableExists(tableName: "documents")
            }
        }
        createTable(tableName: "documents") {
            column(autoIncrement: "true", name: "id", type: '${id.type}') {
                constraints(nullable: "false", primaryKey: "true", primaryKeyName: "documents_PK")
            }

            column(name: "file_type", type: '${string128.type}') {
                constraints(nullable: "false")
            }
            column(name: "category", type: '${string128.type}') {
                constraints(nullable: "false")
            }
            column(name: "content", type: '${blob.type}') {
                constraints(nullable: "false")
            }
            column(name: "upload_date", type: '${datetime.type}') {
                constraints(nullable: "false")
            }
            column(name: "is_read", type: '${boolean.type}') {
                constraints(nullable: "false")
            }
            column(name: "filename", type: '${string128.type}') {
                constraints(nullable: "false")
            }
            column(name: "patient_id", type: '${id.type}') {
                constraints(nullable: "false")
            }

            column(name: "version", type: '${id.type}') { constraints(nullable: "false") }
            column(name: "created_by", type: '${string.type}')
            column(name: "created_date", type: '${datetime.type}')
            column(name: "modified_by", type: '${string.type}')
            column(name: "modified_date", type: '${datetime.type}')
        }
        addForeignKeyConstraint(baseColumnNames: "patient_id", baseTableName: "documents",
                constraintName: "documents_patient_FK",
                referencedColumnNames: "id", referencedTableName: "patient", referencesUniqueColumn: "false")

        createIndex(indexName: "documents_patient_id_idx", tableName: "documents") {
            column(name: "patient_id")
        }
        createIndex(indexName: "documents_upload_date_idx", tableName: "documents") {
            column(name: "upload_date")
        }
    }
}
