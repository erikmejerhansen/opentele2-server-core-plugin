IF COL_LENGTH('[dbo].[patient]','place') IS NULL
BEGIN
  ALTER TABLE [dbo].[patient] ADD [place] NVARCHAR(1024)
END